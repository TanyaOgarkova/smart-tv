import React from 'react';
import styles from './popup.module.css';
import { Link } from 'react-router-dom';

export function Popup(props: {timedPopup: boolean}) {
  return (props.timedPopup) ? (
    <div className={styles.popup}>
         <h1 className={styles.popup__h1}>
            ИСПОЛНИТЕ МЕЧТУ ВАШЕГО МАЛЫША!<br/>
            ПОДАРИТЕ ЕМУ СОБАКУ!
         </h1>
         <div className={styles.popup__qr}/>
         <p className={styles.popup__p}>
            Сканируйте QR-код <br/>
            или нажмите ОК
         </p>
         <Link to='/PhoneNumber'>
            <button className={styles.popup__btn}>
               ОК
            </button>
         </Link>
    </div>
  ): <></>;
}
