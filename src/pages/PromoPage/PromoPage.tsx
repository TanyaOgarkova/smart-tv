import {useState, useEffect} from 'react';
import promo from '../../assets/promo.mp4';
import styles from './promoPage.module.css'
import { Popup } from './Popup';

export function PromoPage() {
  const [timedPopup, setTimedPopup] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setTimedPopup(true);
    }, 5000);
  }, []);

  return (
     <div className={styles.container}>
        <video className ={styles.video} src={promo} width="1280" height="720" autoPlay loop muted/>
        <Popup timedPopup={timedPopup}/>  
    </div>
  )
};



