import * as React from 'react';
import styles from './phoneNumberPage.module.css'
import { PhoneForm } from './PhoneForm';
import { Preview } from './Preview';


export function PhoneNumberPage() {
  const crossRef = React.useRef(null)

  return (
    <div className={styles.container}>
      <PhoneForm crossRef={crossRef} />
      <Preview crossRef={crossRef} />
    </div>
  )
};

