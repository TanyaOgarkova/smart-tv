import styles from './preview.module.css';
import { Link } from 'react-router-dom';

export function Preview(props: {crossRef: any}) {
  return (
    <>
      <div className={styles.preview} />
      <Link ref={props.crossRef} to='/'>
        <button className={styles.preview__closeBtn}/>
      </Link>
      <div className={styles.preview__infoBlock}>
          <div className={styles.preview__p}>
              СКАНИРУЙТЕ QR-КОД ДЛЯ ПОЛУЧЕНИЯ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ
          </div>
          <div className={styles.preview__qr} />
      </div>
    </>
  );
}
