import React from 'react';
import styles from './promoFinal.module.css';

export function PromoFinal() {
  return (
    <div className={styles.container}>
      <h1 className={styles.container__h1}>ЗАЯВКА <br /> ПРИНЯТА</h1>
      <p className={styles.container__p}>Держите телефон под рукой. <br /> Скоро с Вами свяжется наш менеджер. </p>
    </div>
  );
}
