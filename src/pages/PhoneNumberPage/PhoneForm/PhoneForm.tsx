import { useCallback, useEffect, useRef, useState } from 'react'
import styles from './phoneForm.module.css'
import { KeyBoard } from './KeyBoard'
import { CheckBox } from './CheckBox'
import { IMaskInput } from 'react-imask'
import { PromoFinal } from './PromoFinal'
import axios from 'axios'


export function PhoneForm(props: {crossRef:any}) {
  const refMap = [
    [props.crossRef],
    [useRef(null), useRef(null), useRef(null)],
    [useRef(null), useRef(null), useRef(null)],
    [useRef(null), useRef(null), useRef(null)],
    [useRef(null), useRef(null)],
    [useRef<any>(null)],
    [useRef<any>(null)],
  ]
  const [xPos, setXPos] = useState(0)
  const [yPos, setYPos] = useState(1)
  const ref = useRef(null)
  const inputRef = useRef<HTMLInputElement>(null)
  const [checked, setChecked] = useState(false)
  const [complete, setComplete] = useState(false)
  const [inpVal, setInpVal] = useState('')
  const [onClicked, setOnClicked] = useState(false)
  const [isValid, setIsValid] = useState(false)
  const [showAlert, setShowAlert] = useState(false)

  const handleArrowPress = useCallback((event: KeyboardEvent) => {
    switch (event.key) {
      case 'ArrowDown': {
        const posY = yPos<refMap.length-1?yPos+1:0
        setYPos(posY)
        const posX = refMap[posY].length <= xPos? refMap[posY].length-1: xPos 
        setXPos(posX)
        refMap[posY][posX].current.focus()
        break
      }
      case 'ArrowUp': {
        const posY = yPos>0?yPos-1:refMap.length -1
        setYPos(posY)
        const posX = refMap[posY].length <= xPos? refMap[posY].length-1: xPos 
        setXPos(posX)
        refMap[posY][posX].current.focus()
        break
      }
      case 'ArrowLeft': {
        const posX = xPos>0?xPos-1:refMap[yPos].length -1
        setXPos(posX)
        refMap[yPos][posX].current.focus()
        break
      }
      case 'ArrowRight': {
        const posX = xPos<refMap[yPos].length-1?xPos+1:0
        setXPos(posX)
        refMap[yPos][posX].current.focus()
        break
      }
    }
  }, [xPos, yPos, refMap])

  const handleNumberPress = (event: KeyboardEvent) => {
    if ( !(inputRef.current === document.activeElement)){
      if (event.code === 'Backspace'){
        setInpVal(inpVal.slice(0, -1))
      }
      if (event.code.startsWith('Digit')) {
        setInpVal(inpVal + event.key)
      }
    }
  }

  useEffect(
    () => {
      if (refMap[6][0].current){
        if (isValid) refMap[6][0].current.className = styles.form__btnActive
        else{
          if (refMap[6][0].current.className === styles.form__btnActive) {
            refMap[6][0].current.className = styles.form__btn
          }
        }
      }
    },
    [isValid, refMap]
  )
  useEffect(()=> {
    if (checked && complete) {
      axios.get(`http://apilayer.net/api/validate?access_key=${process.env.REACT_APP_API_KEY}&number=${inpVal}&format=1`)
      .then(function (response) {
        if (response.data.valid){
          if (inputRef.current) inputRef.current.style.color="black"
          setIsValid(true)
          setShowAlert(false)
        }
        else {
          if (inputRef.current) inputRef.current.style.color="red"
          setIsValid(false)
          setShowAlert(true)
        }
      })
      .catch(function (error) {
        if (inputRef.current) inputRef.current.style.color="red"
        console.log(error)
        setIsValid(false)
        setShowAlert(true)
      })
    }
    else {
      if (inputRef.current) inputRef.current.style.color="black"
      setIsValid(false)
      setShowAlert(false)
    }
  }, [complete, checked, inpVal])
  useEffect(() => {
    window.addEventListener('keyup', handleArrowPress)
    window.addEventListener('keydown', handleNumberPress)
  
    return () => {
      window.removeEventListener('keyup', handleArrowPress)
      window.removeEventListener('keydown', handleNumberPress)
    }
  }, [handleArrowPress])
  
  return (
    <div className={styles.form}>
      { !onClicked ?
      <>
        <h1 className={styles.form__h1}>Введите ваш номер мобильного телефона</h1>
        <IMaskInput
          className={styles.form__inputTel}
          mask="+7(000)000-00-00"
          placeholderChar='_'
          lazy={false}
          value={inpVal}
          ref={ref}
          inputRef={inputRef}
          color='red'
          onComplete={(value) => {
              setComplete(true)
            }          
          }
          onAccept={(value: string) => {
            if (value.includes("_")) setComplete(false)
            setInpVal(value)
            }
          }
          placeholder=''
        />
        <p className={styles.form__p}>и с Вами свяжется наш менеждер для дальнейшей консультации</p>
        <KeyBoard setInpVal={setInpVal} inpVal={inpVal} refMap={refMap} />
        {showAlert? <h1 className={styles.form__alert}>Неверно введён номер</h1>:<CheckBox setChecked={setChecked} checkRef={refMap[5][0]}/>}
        <button onKeyDown={
          (event)=> {if (event.key==='Enter' && !(checked && complete))event.preventDefault()}
        } onClick={() => setOnClicked(true)} ref={refMap[6][0]} className={styles.form__btn}
          >Подтвердить номер</button>
        </> :  <PromoFinal /> 
       } 
    </div>
    
  )
}
