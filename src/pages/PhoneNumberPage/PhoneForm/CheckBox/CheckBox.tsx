import { RefObject } from 'react';
import styles from './checkBox.module.css';

export function CheckBox(props: {setChecked: Function, checkRef: RefObject<HTMLInputElement>}) {
  return (
   <div className={styles.checkBox}>
      <input ref={props.checkRef} id="check" type='checkbox' className={styles.checkBox__custom}
      onKeyDown={(event) => {
        if (event.key==='Enter' && props.checkRef.current){
          props.checkRef.current.checked = !props.checkRef.current.checked;
          props.setChecked(props.checkRef.current.checked);
        }
        }}
      onChange={(value) => {
        props.setChecked(value.target.checked);
        }}/>
      <label htmlFor="check"> 
        <div className={styles.checkBox__agreement}>  
           Согласие на обработку <br /> персональных данных    
         </div> 
      </label>
   </div>
  );
}
