import styles from './keyBoard.module.css';


export function KeyBoard (props: {setInpVal: Function, inpVal: string, refMap: Array<any>}) {

  function inputChange(event: any){
    props.setInpVal(props.inpVal + event.target.innerText)
  };
  function inputClear(){
    props.setInpVal(props.inpVal.slice(0, -1))
  }

  return (
    <div className={styles.form}>
        <button ref={props.refMap[1][0]} onClick={inputChange} className={styles.form__keyboard}>1</button>
        <button ref={props.refMap[1][1]} onClick={inputChange} className={styles.form__keyboard}>2</button>
        <button ref={props.refMap[1][2]} onClick={inputChange} className={styles.form__keyboard}>3</button>
        
        <button ref={props.refMap[2][0]} onClick={inputChange} className={styles.form__keyboard}>4</button>
        <button ref={props.refMap[2][1]} onClick={inputChange} className={styles.form__keyboard}>5</button>
        <button ref={props.refMap[2][2]} onClick={inputChange} className={styles.form__keyboard}>6</button>
        
        <button ref={props.refMap[3][0]} onClick={inputChange} className={styles.form__keyboard}>7</button>
        <button ref={props.refMap[3][1]} onClick={inputChange} className={styles.form__keyboard}>8</button>
        <button ref={props.refMap[3][2]} onClick={inputChange} className={styles.form__keyboard}>9</button>

        <button ref={props.refMap[4][0]} onClick={inputClear} className={styles.form__clear}>Стереть</button>
        <button ref={props.refMap[4][1]} onClick={inputChange} className={styles.form__keyboard}>0</button>
    </div>
  );
}
