import React from 'react';
import {Routes, Route} from 'react-router-dom'
import { PromoPage } from './pages/PromoPage/PromoPage';
import { PhoneNumberPage } from './pages/PhoneNumberPage/PhoneNumberPage';


function App() {
  return (
   <Routes>
      <Route  path='/' element = {<PromoPage/>}/>
      <Route  path='/PhoneNumber' element = {<PhoneNumberPage/>}/>
   </Routes>
  );
}

export default App;
